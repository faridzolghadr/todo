import React from 'react';
import EditTodo from './edit-todo';
import { todosService } from "./todos-service";

function Todo(props: any) {
    const { id, text, done, editAble } = props.data;
    const deleteIem = () => todosService.delete(id);
    const toggle = () => todosService.toggle(id);
    const editState = () => todosService.editState(id);

    return (
        <React.Fragment>
            {!editAble ? <li className={done ? 'checked' : ''} onDoubleClick={() => editState()}>
                <input type="checkbox" className='check-box' checked={done} onChange={() => toggle()} />
                {text} <span className="close" onClick={() => deleteIem()}>×</span>
            </li> : <EditTodo data={props.data}></EditTodo>
            }

        </React.Fragment>
    )
}

export default Todo