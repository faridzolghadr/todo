import React, { useEffect, useState } from 'react';
import { todosService } from "./todos-service";

function EditTodo(props: any) {
    const [editData, setEditData] = useState('');
    const { id, text, editAble } = props.data;

    useEffect(() => {
        setEditData(editAble ? text : '');
    }, [editAble])
    const editState = () => todosService.editState(id);
    const saveEdit = () => todosService.saveEdit(id, editData);

    return (
        <React.Fragment>
            <li onDoubleClick={() => editState()}>
                <input type="text" value={editData} placeholder="Title..." className='edit' onInput={(e) => setEditData(e.target.value)} />
                <span className="edit-btn" onClick={() => saveEdit()}>Save</span>
            </li>

        </React.Fragment>
    )
}

export default EditTodo