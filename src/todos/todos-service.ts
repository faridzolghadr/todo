import { Subject } from "rxjs";
import { TodosModel, TypeTodo } from './models';

class TodosService {

    private localStorageName: string = 'todos';
    private todosItems: TodosModel[] = [];
    private tempTodosItems: TodosModel[] = [];
    todosSubject$ = new Subject<boolean>();
    isActiveShow: TypeTodo = TypeTodo.all;

    getTodos(type: TypeTodo = TypeTodo.all) {
        if (localStorage.getItem(this.localStorageName)) {
            let data = JSON.parse(localStorage.getItem(this.localStorageName)!);
            this.tempTodosItems = [...data];
            switch (type) {
                case TypeTodo.active:
                    this.todosItems = [...data.filter((it: TodosModel) => !it.done)];
                    break;
                default:
                    this.todosItems = [...data];
                    break;
            }
            this.isActiveShow = type;
        }
        return this.todosItems;
    }

    add(text: string) {
        let timeNow = new Date().getTime() / 1000;
        this.todosItems.push({
            text,
            id: timeNow,
            done: false,
            editAble: false
        });
        this.setTolocalStorage();
    }

    delete(key: number) {
        let indexTodo = this.tempTodosItems.findIndex(todo => todo.id === key);
        this.tempTodosItems.splice(indexTodo, 1);
        this.todosItems = [...this.tempTodosItems];
        this.setTolocalStorage();
    }

    toggle(key: number) {
        let indexTodo = this.tempTodosItems.findIndex(todo => todo.id === key);
        this.tempTodosItems[indexTodo].done = !this.tempTodosItems[indexTodo].done;
        this.todosItems = [...this.tempTodosItems];
        this.setTolocalStorage();
    }

    editState(key: number) {
        let indexTodo = this.todosItems.findIndex(todo => todo.id === key);
        this.todosItems[indexTodo].editAble = !this.todosItems[indexTodo].editAble;
        this.setTolocalStorage();
    }

    saveEdit(key: number, text: string) {
        let indexTodo = this.todosItems.findIndex(todo => todo.id === key);
        this.todosItems[indexTodo].editAble = false;
        this.todosItems[indexTodo].text = text;
        this.setTolocalStorage();
    }

    TodoFilter(type: TypeTodo) {
        this.todosSubject$.next(false);
        this.getTodos(type);
        this.isActiveShow = type;
        this.todosSubject$.next(true);
    }

    clearCompleted() {
        let tempTodo: TodosModel[] = [];
        for (let index = 0; index < this.todosItems.length; index++) {
            const item = this.todosItems[index];
            if (!item.done) tempTodo.push(item);
        }
        this.todosItems = [];
        this.todosItems = [...tempTodo];
        this.setTolocalStorage();
    }

    private setTolocalStorage() {
        this.todosSubject$.next(false);
        localStorage.removeItem(this.localStorageName);
        localStorage.setItem(this.localStorageName, JSON.stringify(this.todosItems));
        this.todosSubject$.next(true);
    }

}

export const todosService = new TodosService();