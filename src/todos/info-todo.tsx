import { TodosModel, TypeTodo } from "./models";
import { todosService } from "./todos-service";

function InfoTodo(props: any) {
    const { todos } = props;
    return (
        <div className='main-info'>
            <div><strong>{todos?.filter((it: TodosModel) => !it.done).length}</strong> item left</div>
            <div>
                <span className='filter-todo' onClick={() => todosService.TodoFilter(TypeTodo.all)}>
                    <span className={todosService.isActiveShow === TypeTodo.all ? 'filter-todo-active' : ''}>All</span>
                </span>
                <span className='filter-todo' onClick={() => todosService.TodoFilter(TypeTodo.active)}>
                    <span className={todosService.isActiveShow === TypeTodo.active ? 'filter-todo-active' : ''}>Active</span>
                </span>
            </div>
            <div>
                <span className='btn-clear-complated' onClick={() => todosService.clearCompleted()}>Clear completed</span>
            </div>
        </div>
    )
}

export default InfoTodo