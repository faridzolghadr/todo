export interface TodosModel {
    id: number;
    text: string;
    done: boolean;
    editAble: boolean;
}

export enum TypeTodo {
    all = 'all',
    active = 'active',
}