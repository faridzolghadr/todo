import React, { useState } from 'react';
import { todosService } from "./todos-service";

function CreateTodos() {
    const [text, setText] = useState('');
    const addTodo = () => {
        if (!text) return;
        todosService.add(text);
        setText('');
    }

    return (
        <React.Fragment>
            <form className="header">
                <h2>To Do List</h2>
                <input type="text" name='text' placeholder="Title..." value={text} onChange={(e) => setText(e.target.value)} />
                <input type="submit" onClick={() => addTodo()} className="addBtn" disabled={!text} />
            </form>
        </React.Fragment>
    )
}

export default CreateTodos