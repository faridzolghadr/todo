import React, { useEffect, useState } from 'react';
import CreateTodos from './create-todo';
import InfoTodo from './info-todo';
import { TodosModel } from './models';
import Todo from './todo';
import { todosService } from "./todos-service";

function Todos() {
    const [todos, setTodos] = useState<TodosModel[]>([]);
    useEffect(() => {
        setTodos([...todosService.getTodos(todosService.isActiveShow)]);
        todosService.todosSubject$.subscribe(result => {
            if (result) {
                setTodos([...todosService.getTodos(todosService.isActiveShow)]);
            }
        })
    }, []);

    return (
        <React.Fragment>
            <div className='main'>
                <CreateTodos />
                <ul>
                    <li className='headert-list'>What needs to be done?</li>
                    {todos?.length > 0 ? todos.map((it, index) => {
                        return <Todo key={index} data={it} />
                    }) : <li className='text-center'>There is no data for show!</li>}

                </ul>
            </div>
            <InfoTodo todos={todos} />
        </React.Fragment>
    )
}

export default Todos