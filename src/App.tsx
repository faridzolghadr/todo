import React, { useState } from 'react'
import './App.css'
import Todos from './todos/todos'

function App() {

  return (
    <React.Fragment>
      <Todos></Todos>
    </React.Fragment>
  )
}

export default App
